# from rest_framework.decorators import api_view, permission_classes
from rest_framework.filters import SearchFilter, OrderingFilter
import random
from rest_framework import status
from rest_framework.generics import GenericAPIView, CreateAPIView, RetrieveAPIView
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from users.models import ConfirmCode

from users.serializers import (LoginSerializer,
                               RegisterSerializer,
                               UserSerializer)


class LoginAPIView(GenericAPIView):
    serializer_class = LoginSerializer

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        user = authenticate(**serializer.data)

        if user:
            token, created = Token.objects.get_or_create(user=user)
            return Response({'token': token.key})
        return Response({'ERROR': 'WRONG CREDENTIALS'}, status=400)


class LogoutAPIView(GenericAPIView):
    permission_classes = [IsAuthenticated]  # Добавлена проверка аутентификации

    def delete(self, request):
        request.auth.delete()  # Метод delete для удаления токена
        return Response(
            {'message': 'You have been successfully logged out.'},
            status=status.HTTP_200_OK
        )


class RegisterAPIView(CreateAPIView):
    serializer_class = RegisterSerializer

    def post(self, request, *args, **kwargs):
        serializer = RegisterSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        code = ''.join(random.choice('1234567890') for _ in range(6))

        user = User.objects.create_user(is_active=False, **serializer.validated_data)
        ConfirmCode.objects.create(user=user, code=code)

        token, created = Token.objects.get_or_create(user=user)

        return Response({'token': token.key, 'data': serializer.data}, status=201)


class ConfirmAPIView(CreateAPIView):
    def post(self, request, *args, **kwargs):
        code = request.data.get('code')
        if not code:
            return Response({'ERROR': 'NO CODE'}, status=400)

        code = ConfirmCode.objects.filter(code=code).first()

        if not code:
            return Response({'ERROR': 'NO CODE'}, status=400)

        user = code.user
        user.is_active = True
        user.save()
        code.delete()

        return Response({'message': 'Account has been confirmed'}, status=200)


class ProfileAPIView(RetrieveAPIView):
    serializer_class = UserSerializer
    permission_classes = [IsAuthenticated]

    def get_object(self):
        return self.request.user

# @api_view(['POST'])
# def login(request):
#     # 0 - валидация
#     serializer = LoginSerializer(data=request.data)
#     serializer.is_valid(raise_exception=True)
#
#     # 2 - найти пользователя в базе данных
#     user = authenticate(**serializer.data)  # serializer.data -> {username:password}
#     ConfirmCode.objects.create(user=user)
#     # 3 - создать токен
#     if user:
#         token, created = Token.objects.get_or_create(user=user)
#
#         # 4 - отправить токен
#         return Response({'token': token.key})
#     return Response({'ERROR': 'WRONG CREDENTIALS'}, status=400)
# @api_view(['GET'])
# @permission_classes([IsAuthenticated])
# def logout(request):
#     request.user.auth_token.delete()
#     return Response(
#         {'massage': 'You have been successfully logged out.'},
#         status=200
#     )
# @api_view(['POST'])
# def register(request):
#     serializer = RegisterSerializer(data=request.data)
#     serializer.is_valid(raise_exception=True)
#     user = User.objects.create_user(is_active=False, **serializer.data)
#     code = ''.join(random.choice('1234567890')for _ in range(6))
#     ConfirmCode.objects.create(user=user, code=code)
#     token, created = Token.objects.get_or_create(user=user)
#     return Response({'token': token.key, 'data': serializer.data}, status=201)


# @api_view(['POST'])
# def register(request):
#     # 0 - валидация
#     serializer = RegisterSerializer(data=request.data)
#     serializer.is_valid(raise_exception=True)
#
#     # 1 - создать пользователя
#     user = User.objects.create_user(is_active=False, **serializer.data)
#     SmsCode.objects.create(user=user, code=randint(1000, 9999))
#
#     # 2 - создать токен
#     token, created = Token.objects.get_or_create(user=user)

#
#     return Response(
#         {'token': token.key, 'data': serializer.data},
#         status=201
#     )
#
# @api_view(['POST'])
# def confirm(request):
#     code = request.data.get('code')
#     code_obj = SmsCode.objects.filter(code=code).first()
#     if not code_obj:
#         return Response({'error': 'wrong code'}, status=400)
#
#     code_obj.user.is_active = True
#     code_obj.user.save()
#     code_obj.delete()
#
#     return Response({'message': 'success'}, status=200)
# @api_view(['GET'])
# @permission_classes([IsAuthenticated])
# def profile(request):
#     user = request.user
#     serializer = UserSerializer(user)
#     return Response({'data': serializer.data}, status=200)
