from django.urls import path
from users import views

urlpatterns = [
    path('api/v1/users/login/', views.LoginAPIView.as_view()),
    path('api/v1/users/logout/', views.LogoutAPIView.as_view()),
    path('api/v1/users/register/', views.RegisterAPIView.as_view()),
    path('api/v1/users/profile/', views.ProfileAPIView.as_view()),
    path('api/v1/users/confirm/', views.ConfirmAPIView.as_view()),
]
