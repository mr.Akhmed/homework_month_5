from movie_app.models import Director, Movie, Review
# from rest_framework.pagination import PageNumberPagination

from rest_framework.generics import (
    ListCreateAPIView,
    ListAPIView,
    RetrieveUpdateDestroyAPIView
)
from movie_app.serializers import (
    DirectorSerializer,
    MoviesSerializer,
    ReviewSerializer,
    AverageSerializer,
)


class DirectorListAPIView(ListCreateAPIView):
    queryset = Director.objects.all()
    serializer_class = DirectorSerializer
    pagination_class = None


class MoviesListAPIView(ListCreateAPIView):
    queryset = Movie.objects.all()
    serializer_class = MoviesSerializer


class ReviewListAPIView(ListCreateAPIView):
    queryset = Review.objects.all()
    serializer_class = ReviewSerializer
    # pagination_class = PageNumberPagination


class MovieReviewAPIView(ListAPIView):
    queryset = Review.objects.all()
    serializer_class = AverageSerializer


class DirectorsDetailAPIView(RetrieveUpdateDestroyAPIView):
    queryset = Director.objects.all()
    serializer_class = DirectorSerializer


class MoviesDetailAPIView(RetrieveUpdateDestroyAPIView):
    queryset = Movie.objects.all()
    serializer_class = MoviesSerializer


class ReviewDetailAPIView(RetrieveUpdateDestroyAPIView):
    queryset = Review.objects.all()
    serializer_class = ReviewSerializer
    lookup_field = 'id'

# from django.db.models import Count
# from rest_framework.decorators import api_view
# from rest_framework.response import Response
# добавление режиссеров :
# @api_view(['GET', 'POST'])
# def director_view(request):
#     if request.method == 'GET':
#         directors = Director.objects.annotate(movies_count=Count('movies'))
#         serializer = DirectorSerializer(instance=directors, many=True)
#         return Response(serializer.data)
#     # создание
#     elif request.method == 'POST':
#         serializer = DirectorValidateSerializer(data=request.data)
#         # способ 1
#         # if not serializer.is_valid():
#         #     return Response(serializer.errors, status=400)
#         # способ 2
#         serializer.is_valid(raise_exception=True)
#         # name = request.data['name']  # не рекомендуется
#
#         # name = serializer.validated_data.get('name')
#         directors = Director.objects.create(
#             name=serializer.validated_data.get('name')
#         )
#         serializer = DirectorSerializer(instance=directors, many=False)  # instance принимает QuerySet
#         return Response(serializer.data)


# добавление фильмов:
# @api_view(['GET', 'POST'])
# def movies_view(request):
#     if request.method == "GET":
#         movies = Movie.objects.all()
#         data = MoviesSerializer(instance=movies, many=True).data
#         return Response(data)
#     # Добавлеие:
#     elif request.method == "POST":
#         serializer = MovieValidateSerializer(data=request.data)  # data принимает словарь
#         serializer.is_valid(raise_exception=True)
#
#         movies = Movie.objects.create(
#             title=serializer.validated_data.get('title'),  # возвращает валидированый словарь
#             description=serializer.validated_data.get('description'),
#             duration=serializer.validated_data.get('duration'),
#             director_id=serializer.validated_data.get('director_id')
#         )
#         serializer = MoviesSerializer(instance=movies, many=False)
#         return Response(serializer.data)

# добавление отзывов:
# @api_view(['GET', 'POST'])
# def reviews_view(request):
#     if request.method == "GET":
#         reviews = Review.objects.all()
#         serializer = ReviewSerializer(reviews, many=True)
#         return Response(serializer.data)
#     # Добавление
#     elif request.method == "POST":
#         serializer = ReviewValidateSerializer(data=request.data)
#         serializer.is_valid(raise_exception=True)
#
#         reviews = Review.objects.create(
#             text=serializer.validated_data.get('text'),
#             movie_id=serializer.validated_data.get('movie_id'),
#             rating=serializer.validated_data.get('rating'))
#         serializer = ReviewSerializer(instance=reviews, many=False)
#         return Response(serializer.data)
#


# Вью для показание средний бал
# @api_view(['GET'])
# def movie_reviews(request):
#     reviews = Review.objects.all()
#     serializer = AverageSerializer(instance=reviews, many=True)
#     return Response(serializer.data)

# 1 способ =>
# news.tags.set(tags)
# 2 способ
# for tag in tags:
#     news.tags.add(tag_id)
# 3 способ for tag
# news.tags.add(*tags)


# Изменение и удаление:
# @api_view(['GET', 'PUT', 'DELETE'])
# def directors_view_id(request, id):
#     try:
#         directors = Director.objects.get(id=id)
#     except Director.DoesNotExist:
#         return Response({'ERROR': f"Режиссер с id {id} не существует"})
#     if request.method == 'GET':
#         serializer = DirectorSerializer(instance=directors)
#         return Response(serializer.data, status=200)
#     # Изменение
#     elif request.method == 'PUT':
#         serializer = DirectorValidateSerializer(data=request.data)
#         serializer.is_valid(raise_exception=True)
#
#         directors.name = serializer.validated_data.get('name', directors.name)
#         directors.save()
#         serializer = DirectorSerializer(instance=directors)
#         return Response(serializer.data)
#     # Удаление
#     elif request.method == 'DELETE':
#         directors.delete()
#         return Response(status=204)
# @api_view(['GET', 'PUT', 'DELETE'])
# def movies_view_id(request, movies_id):
#     try:
#         movies = Movie.objects.get(id=movies_id)
#     except Movie.DoesNotExist:
#         return Response({'ERROR': f"Фильм с id {movies_id} не существует"})
#     if request.method == "GET":
#         serializer = MoviesSerializer(instance=movies)
#         return Response(serializer.data)
#     # Изменение
#     elif request.method == "PUT":
#         serializer = MovieValidateSerializer(data=request.data)
#         serializer.is_valid(raise_exception=True)
#         movies.title = serializer.validated_data.get('title', movies.title)
#         movies.description = serializer.validated_data.get('description', movies.description)
#         movies.duration = serializer.validated_data.get('duration', movies.duration)
#         movies.director_id = serializer.validated_data.get('director_id', movies.director_id)
#         movies.save()
#         serializer = MoviesSerializer(instance=movies)
#         return Response(serializer.data)
#     # Удаление
#     elif request.method == 'DELETE':
#         movies.delete()
#         return Response(status=204)
# @api_view(['GET', 'PUT', 'DELETE'])
# def reviews_view_id(request, id):
#     try:
#         reviews = Review.objects.get(id=id)
#     except Review.DoesNotExist:
#         return Response({'ERROR': f" с такой id {id} не существует"})
#     if request.method == 'GET':
#         serializer = ReviewSerializer(instance=reviews)
#         return Response(serializer.data)
#     # Изменение
#     elif request.method == 'PUT':
#         serializer = ReviewValidateSerializer(data=request.data)
#         serializer.is_valid(raise_exception=True)
#         reviews.text = serializer.validated_data.get('text', reviews.text)
#         reviews.movie_id = serializer.validated_data.get('movie_id', reviews.movie_id)
#         reviews.rating = serializer.validated_data.get('rating', reviews.rating)
#         reviews.save()
#         serializer = ReviewSerializer(instance=reviews)
#         return Response(serializer.data)
#     # Удавление
#     elif request.method == 'DELETE':
#         reviews.delete()
#         return Response(status=204)
