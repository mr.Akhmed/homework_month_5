from django.db import models
from django.db.models import Avg


class Director(models.Model):
    name = models.TextField(max_length=100, null=True)

    class Meta:
        verbose_name = 'режиссера'
        verbose_name_plural = 'Режиссер'

    def __str__(self):
        return self.name


class Movie(models.Model):
    title = models.CharField(max_length=100)
    description = models.TextField()
    duration = models.IntegerField()
    director = models.ForeignKey(Director, on_delete=models.CASCADE, null=True, related_name='movies')

    class Meta:
        verbose_name = 'фильм'
        verbose_name_plural = 'Фильмы'

    def __str__(self):
        return self.title


class Review(models.Model):
    STARS_CHOICES = ((i, i * '*') for i in range(1, 6))
    text = models.TextField(max_length=100)
    movie = models.ForeignKey(Movie, on_delete=models.CASCADE, related_name='reviews')
    rating = models.PositiveSmallIntegerField(choices=STARS_CHOICES)

    class Meta:
        verbose_name = 'отзыв'
        verbose_name_plural = 'Отзывы'

    def __str__(self):
        return self.text