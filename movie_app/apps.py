from django.apps import AppConfig


class AfishaConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'movie_app'
    verbose_name = 'Все об фильмах'
