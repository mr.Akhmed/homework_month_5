from django.contrib import admin
from django.http.request import HttpRequest
from movie_app.models import Director, Movie, Review


# admin.site.register(models.Director)
@admin.register(Director)
class DirectorAdmin(admin.ModelAdmin):
    list_display = ('id', 'name',)
    list_display_links = ('name',)

    # def has_add_permission(self, request: HttpRequest) -> bool:
    #     if request.user.is_superuser:
    #         return True
    #     return False

    def has_add_permission(self, request: HttpRequest) -> bool:
        return True


# admin.site.register(models.Movie)
@admin.register(Movie)
class MovieAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'description', 'duration', 'director')
    list_display_links = ('title',)


# admin.site.register(models.Review)
@admin.register(Review)
class ReviewAdmin(admin.ModelAdmin):
    list_display = ('id', 'text', 'movie', 'rating')
    list_display_links = ('text',)
